package de.zimmermann.spring.security.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityController {

  //доступ любому пользователю
  @GetMapping("/login")
  public String loginResponse() {
    return "Login";
  }

  //доступ админу
  @GetMapping("/admin")
  public String adminEndpoint() {
    return "Admin!";
  }

  //доступ обыкновенному пользователю
  @GetMapping("/user")
  public String userEndpoint() {
    return "User!";
  }

}
